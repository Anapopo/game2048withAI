#-------------------------------------------------
#
# Project created by QtCreator 2014-03-13T19:50:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 2048
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    grid.cpp \
    ai.cpp

HEADERS  += mainwindow.h \
    grid.h \
    ai.h

FORMS    += mainwindow.ui

RESOURCES += \
    src.qrc
