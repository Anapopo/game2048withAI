#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    timeTillNow(0),
    grid(),
    ai(&grid),
    aiPlay(false),
    ui(new Ui::MainWindow)
{ 
    ui->setupUi(this);
    connect(ui->actionRestart, &QAction::triggered, this, &MainWindow::Restart);
    connect(ui->actionUndo, &QAction::triggered, this, &MainWindow::Undo);
    connect(ui->pushButton, &QPushButton::clicked,this, &MainWindow::AiSwitch);
    ShowItem(grid.GetGrid());

    timer = new QTimer(this);
    aiTimer = new QTimer(this);


    ui->score->display(grid.GetScore());

    ui->timer->display(timeTillNow);

    connect(timer,SIGNAL(timeout()),this,SLOT(updateTimer()));
    connect(aiTimer,SIGNAL(timeout()),this,SLOT(GetMove()));

    timer->start(1000);
    aiTimer->start(200);


}

void MainWindow::GetMove(){
    if(aiPlay){
        ui->move->display(DoMove());
        if(ai.count > 0){
            ai.count -- ;
        }
        AddNumber();
    }
}

int MainWindow::DoMove(){
    if(aiPlay){
        switch (ai.GetBest(4,-10000,10000).first){
        case 1:
            HandleUp();
            return 1;
        case 2:
            HandleRight();
            return 2;
        case 3:
            HandleDown();
            return 3;
        case 4:
            HandleLeft();
            return 4;
        default:
            return 5;
        }
    }
    else
        return 5;
}

MainWindow::~MainWindow()
{
    delete timer;
    delete aiTimer;
    delete ui;
}

void MainWindow::updateLCD(){
    ui->score->display(grid.GetScore());
}
void MainWindow::updateTimer(){
    ui->timer->display(++timeTillNow);
}

void MainWindow::keyPressEvent(QKeyEvent *k ){
    if(aiPlay){
        return;
    }
    if(k->key() == Qt::Key_W ||k->key() == Qt::Key_Up){
        grid.SetLastPress(Grid::UP);
        HandleUp();
    }else if(k->key() == Qt::Key_S || k->key() == Qt::Key_Down){
        grid.SetLastPress(Grid::DOWN);
        HandleDown();
    }else if(k->key() == Qt::Key_D || k->key() == Qt::Key_Right){
        grid.SetLastPress(Grid::RIGHT);
        HandleRight();
    }else if(k->key() == Qt::Key_A || k->key() == Qt::Key_Left){
        grid.SetLastPress(Grid::LEFT);
        HandleLeft();
    }else{
        return;
    }
    AddNumber();
}

void MainWindow::ShowItem(int* t){
    ui->label->setPixmap(QPixmap(Display(t[0])));
    ui->label_2->setPixmap(QPixmap(Display(t[1])));
    ui->label_3->setPixmap(QPixmap(Display(t[2])));
    ui->label_4->setPixmap(QPixmap(Display(t[3])));
    ui->label_5->setPixmap(QPixmap(Display(t[4])));
    ui->label_6->setPixmap(QPixmap(Display(t[5])));
    ui->label_7->setPixmap(QPixmap(Display(t[6])));
    ui->label_8->setPixmap(QPixmap(Display(t[7])));
    ui->label_9->setPixmap(QPixmap(Display(t[8])));
    ui->label_10->setPixmap(QPixmap(Display(t[9])));
    ui->label_11->setPixmap(QPixmap(Display(t[10])));
    ui->label_12->setPixmap(QPixmap(Display(t[11])));
    ui->label_13->setPixmap(QPixmap(Display(t[12])));
    ui->label_14->setPixmap(QPixmap(Display(t[13])));
    ui->label_15->setPixmap(QPixmap(Display(t[14])));
    ui->label_16->setPixmap(QPixmap(Display(t[15])));

//    ui->label->setNum(t[0]);
//    ui->label_2->setNum(t[1]);
//    ui->label_3->setNum(t[2]);
//    ui->label_4->setNum(t[3]);
//    ui->label_5->setNum(t[4]);
//    ui->label_6->setNum(t[5]);
//    ui->label_7->setNum(t[6]);
//    ui->label_8->setNum(t[7]);
//    ui->label_9->setNum(t[8]);
//    ui->label_10->setNum(t[9]);
//    ui->label_11->setNum(t[10]);
//    ui->label_12->setNum(t[11]);
//    ui->label_13->setNum(t[12]);
//    ui->label_14->setNum(t[13]);
//    ui->label_15->setNum(t[14]);
//    ui->label_16->setNum(t[15]);
}


QString MainWindow::Display(int temp){
    switch (temp){
    case 0:
        return 0;
    case 2:
        return ":/image/2";
    case 4:
        return ":/image/4";
    case 8:
        return ":/image/8";
    case 16:
        return ":/image/16";
    case 32:
        return ":/image/32";
    case 64:
        return ":/image/64";
    case 128:
        return ":/image/128";
    case 256:
        return ":/image/256";
    case 512:
        return ":/image/512";
    case 1024:
        return ":/image/1024";
    case 2048:
        return ":/image/2048";
    default:
        return 0;
     }
}
void MainWindow::HandleUp(){
    grid.HandleUp();
    ShowItem(grid.GetGrid());
    ui->score->display(grid.GetScore());
}

void MainWindow::HandleDown(){
    grid.HandleDown();
    ShowItem(grid.GetGrid());
    ui->score->display(grid.GetScore());
}

void MainWindow::HandleLeft(){
    grid.HandleLeft();
    ShowItem(grid.GetGrid());
    ui->score->display(grid.GetScore());
}

void MainWindow::HandleRight(){
    grid.HandleRight();
    ShowItem(grid.GetGrid());
    ui->score->display(grid.GetScore());
}


void MainWindow::Restart(){
    grid.Restart();
    ShowItem(grid.GetGrid());
    timeTillNow = 0;
    timer->start();
    ui->score->display(grid.GetScore());
    ui->timer->display(timeTillNow);
}
void MainWindow::Undo(){
    grid.Undo();
    timer->start();
    ui->score->display(grid.GetScore());
    ShowItem(grid.GetGrid());
}

void MainWindow::AddNumber(){
    if(!grid.Equal()){
        grid.AddNumber();
        ShowItem(grid.GetGrid());
    }else{
        grid.CheckLose();
    }
    if(grid.IsLose()){
        ShowLose();
        timer->stop();
    }
}

void MainWindow::ShowLose(){
    ui->label->setPixmap(QPixmap(":/image/0"));
    ui->label_2->setPixmap(QPixmap(":/image/0"));
    ui->label_3->setPixmap(QPixmap(":/image/0"));
    ui->label_4->setPixmap(QPixmap(":/image/0"));
    ui->label_5->setPixmap(QPixmap(":/image/0"));
    ui->label_6->setPixmap(QPixmap(":/image/0"));
    ui->label_7->setPixmap(QPixmap(":/image/0"));
    ui->label_8->setPixmap(QPixmap(":/image/0"));
    ui->label_9->setPixmap(QPixmap(":/image/0"));
    ui->label_10->setPixmap(QPixmap(":/image/0"));
    ui->label_11->setPixmap(QPixmap(":/image/0"));
    ui->label_12->setPixmap(QPixmap(":/image/0"));
    ui->label_13->setPixmap(QPixmap(":/image/0"));
    ui->label_14->setPixmap(QPixmap(":/image/0"));
    ui->label_15->setPixmap(QPixmap(":/image/0"));
    ui->label_16->setPixmap(QPixmap(":/image/0"));
}
